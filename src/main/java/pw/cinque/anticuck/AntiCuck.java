package pw.cinque.anticuck;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import pw.cinque.anticuck.check.Check;
import pw.cinque.anticuck.check.CheckListener;

public class AntiCuck extends JavaPlugin {

    private static final String ALERT_FORMAT = "§e[AntiCuck] §b%s §ffailed §3%s (%d)";

    @Getter
    private static AntiCuck instance;

    @Override
    public void onEnable() {
        instance = this;

        Bukkit.getPluginManager().registerEvents(new CheckListener(), this);
    }

    public void alert(ACPlayer player, Check check, int vl) {
        Bukkit.broadcast(String.format(ALERT_FORMAT, player.getPlayer().getName(), check.getName(), vl), "anticuck.alert");
    }

}
