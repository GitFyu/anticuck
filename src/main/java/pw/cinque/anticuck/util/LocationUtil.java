package pw.cinque.anticuck.util;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.BlockFace;

public class LocationUtil { // pretty much stolen from NCP

    private static final int LOCATION_ON_GROUND = 1;
    private static final int LOCATION_IN_LIQUID = 2;
    private static final int LOCATION_ON_LADDER = 2 << 1;
    private static final int LOCATION_IN_WEB = 2 << 2;
    private static final int LOCATION_BELOW_BLOCK = 2 << 3;
    private static final int LOCATION_ABOVE_GROUND = 2 << 4;

    private static final int TYPE_SOLID = 1;
    private static final int TYPE_LIQUID = 2;
    private static final int TYPE_LADDER = 2 << 1;
    private static final int TYPE_WALL = 2 << 2;

    private static final int[] TYPE_INFO = new int[256];

    static {
        for (int i = 0; i < TYPE_INFO.length; i++) {
            Material material = Material.values()[i];

            if (material.isSolid()) {
                TYPE_INFO[i] = TYPE_SOLID;
            }
        }

        // fix some types where isSolid() returns the wrong value
        TYPE_INFO[Material.SIGN_POST.getId()] = 0;
        TYPE_INFO[Material.WALL_SIGN.getId()] = 0;
        TYPE_INFO[Material.DIODE_BLOCK_OFF.getId()] = TYPE_SOLID;
        TYPE_INFO[Material.DIODE_BLOCK_ON.getId()] = TYPE_SOLID;
        TYPE_INFO[Material.CARPET.getId()] = TYPE_SOLID;
        TYPE_INFO[Material.SNOW.getId()] = TYPE_SOLID;
        TYPE_INFO[Material.ANVIL.getId()] = TYPE_SOLID;

        // liquids
        TYPE_INFO[Material.WATER.getId()] |= TYPE_LIQUID;
        TYPE_INFO[Material.STATIONARY_WATER.getId()] |= TYPE_LIQUID;
        TYPE_INFO[Material.LAVA.getId()] |= TYPE_LIQUID;
        TYPE_INFO[Material.STATIONARY_LAVA.getId()] |= TYPE_LIQUID;

        // ladders
        TYPE_INFO[Material.LADDER.getId()] |= TYPE_LADDER;
        TYPE_INFO[Material.VINE.getId()] |= TYPE_LADDER;

        // walls
        TYPE_INFO[Material.FENCE.getId()] |= TYPE_WALL;
        TYPE_INFO[Material.FENCE_GATE.getId()] |= TYPE_WALL;
        TYPE_INFO[Material.COBBLE_WALL.getId()] |= TYPE_WALL;
        TYPE_INFO[Material.NETHER_FENCE.getId()] |= TYPE_WALL;

        // TODO list is probably incomplete
    }

    private LocationUtil() {
    }

    public static int checkLocation(Location location) {
        int flags = 0;

        int minX = Location.locToBlock(location.getX() - 0.3);
        int minZ = Location.locToBlock(location.getZ() - 0.3);
        double y = location.getY();
        int maxX = Location.locToBlock(location.getX() + 1.3);
        int maxZ = Location.locToBlock(location.getZ() + 1.3);

        World world = location.getWorld();

        for (int x = minX; x != maxX; x += 1) {
            for (int z = minZ; z != maxZ; z += 1) {
                int blockY = Location.locToBlock(y);

                Material type = world.getBlockAt(x, blockY, z).getType();
                Material typeFeet = world.getBlockAt(x, blockY - 1, z).getType();
                Material typeHead = world.getBlockAt(x, blockY + 1, z).getType();
                Material typeAbove = world.getBlockAt(x, blockY + 2, z).getType();

                if (isSolid(typeFeet)) {
                    flags |= (y - blockY < 0.14 || isWall(typeFeet)) ? LOCATION_ON_GROUND : LOCATION_ABOVE_GROUND;
                }

                if (isSolid(type)) {
                    flags |= LOCATION_ON_GROUND;
                } else if (type == Material.WEB || typeFeet == Material.WEB || typeHead == Material.WEB) {
                    flags |= LOCATION_IN_WEB;
                }

                if (isSolid(typeAbove)) {
                    flags |= LOCATION_BELOW_BLOCK;
                }

                if (isLiquid(type) || isLiquid(typeHead)) {
                    flags |= LOCATION_IN_LIQUID;
                }
            }
        }

        Material typeCenter = location.getBlock().getType();
        Material typeCenterAbove = location.getBlock().getRelative(BlockFace.UP).getType();

        if (isLadder(typeCenter) || isLadder(typeCenterAbove)) {
            flags |= LOCATION_ON_LADDER;
        }

        return flags;
    }

    public static boolean isOnGround(int locationInfo) {
        return (locationInfo & LOCATION_ON_GROUND) == LOCATION_ON_GROUND;
    }

    public static boolean isSwimming(int locationInfo) {
        return (locationInfo & LOCATION_IN_LIQUID) == LOCATION_IN_LIQUID;
    }

    public static boolean isClimbingLadder(int locationInfo) {
        return (locationInfo & LOCATION_ON_LADDER) == LOCATION_ON_LADDER;
    }

    public static boolean isInWeb(int locationInfo) {
        return (locationInfo & LOCATION_IN_WEB) == LOCATION_IN_WEB;
    }

    public static boolean isBelowBlock(int locationInfo) {
        return (locationInfo & LOCATION_BELOW_BLOCK) == LOCATION_BELOW_BLOCK;
    }

    public static boolean isAboveGround(int locationInfo) {
        return (locationInfo & LOCATION_ABOVE_GROUND) == LOCATION_ABOVE_GROUND;
    }

    private static boolean isSolid(Material material) {
        return (TYPE_INFO[material.getId()] & TYPE_SOLID) == TYPE_SOLID;
    }

    private static boolean isLiquid(Material material) {
        return (TYPE_INFO[material.getId()] & TYPE_LIQUID) == TYPE_LIQUID;
    }

    private static boolean isLadder(Material material) {
        return (TYPE_INFO[material.getId()] & TYPE_LADDER) == TYPE_LADDER;
    }

    private static boolean isWall(Material material) {
        return (TYPE_INFO[material.getId()] & TYPE_WALL) == TYPE_WALL;
    }

}
