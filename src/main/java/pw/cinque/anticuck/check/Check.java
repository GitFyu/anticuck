package pw.cinque.anticuck.check;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import pw.cinque.anticuck.ACPlayer;
import pw.cinque.anticuck.AntiCuck;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public abstract class Check {

    @Getter
    private final String name;
    private final int maxViolations;
    protected final ACPlayer player;

    private List<Long> violations = new ArrayList<>();

    public abstract void run();

    protected void onViolation(String info) { // info isn't used rn
        if (player.punished) {
            return;
        }

        violations.removeIf(time -> time < System.currentTimeMillis());
        violations.add(System.currentTimeMillis() + 60000L);

        int vl = violations.size();
        AntiCuck.getInstance().alert(player, this, vl);

        if (vl >= maxViolations) {
            player.punished = true;
            player.getPlayer().kickPlayer("Cheating");
        }
    }

}
