package pw.cinque.anticuck.check.move;

import pw.cinque.anticuck.ACPlayer;
import pw.cinque.anticuck.check.Check;

public class TooManyMovesCheck extends Check {

    private long nextCheck;
    private int movesReceived;

    public TooManyMovesCheck(ACPlayer player) {
        super("TooManyMoves", 20, player);
    }

    @Override
    public void run() {
        if (movesReceived++ > 600) { // this shouldn't be possible, even when lagging
            onViolation(movesReceived + " moves/sec");
        }

        long currentTime = System.currentTimeMillis();

        if (nextCheck < currentTime) {
            long timeframe = 1000L + currentTime - nextCheck;
            int maxMoves = Math.min(100, (int) (timeframe / 45.0)); // dividing by 45 instead of 50 (1 tick = 50ms) to prevent the check from being too sensitive

            if (movesReceived > maxMoves) {
                onViolation(movesReceived + " moves/sec");
            }

            nextCheck = currentTime + 1000L;
            movesReceived = 0;
        }
    }

}
