package pw.cinque.anticuck.check.move;

import org.bukkit.Bukkit;
import org.bukkit.potion.PotionEffectType;
import pw.cinque.anticuck.ACPlayer;
import pw.cinque.anticuck.check.Check;

public class FlyCheck extends Check {

    private int remainingMoves;
    private double verticalLimit;
    private int climbViolations;
    private double lastFallSpeed;
    private int fallViolations;
    private int jumpTicks;
    private int landTicks;
    private int nextJump;

    public FlyCheck(ACPlayer player) {
        super("Fly", 30, player);
    }

    @Override
    public void run() {
        int jumpLevel = player.getPotionLevel(PotionEffectType.JUMP);

        if (player.onGround || player.swimming || player.climbingLadder || player.aboveGround) {
            remainingMoves = 6 + jumpLevel;
            verticalLimit = (player.swimming ? 0.8 : 1.3) + jumpLevel * 1.2; // TODO calculate the limit for jump boost in a more accurate way
        }

        if (player.verticalDistance > 0.0) {
            if (player.climbingLadder) {
                if (player.verticalDistance > 0.2 && climbViolations++ >= 3) {
                    onViolation("climbed too fast");
                }
            } else if (player.swimming) {
                if (player.verticalDistance > 0.3 && climbViolations++ >= 3) {
                    onViolation("rose too fast in water");
                }
            } else {
                if (player.verticalVelocity <= 0.0 && --remainingMoves < 0) { // TODO don't fully 'disable' this check if the player has velocity
                    onViolation("too many moves");
                } else if ((verticalLimit -= (player.verticalDistance - player.verticalVelocity)) < 0.0) {
                    onViolation("jumped too high");
                }

                climbViolations = 0;
            }

            lastFallSpeed = 0.0;
        } else {
            if (!player.onGround && !player.inWeb && !player.swimming && !player.climbingLadder) {
                double fallSpeedIncrease = player.verticalDistance - lastFallSpeed;

                if ((fallSpeedIncrease < -0.0785 || fallSpeedIncrease > -0.0001) && fallViolations++ >= 3) {
                    onViolation("illegal fall speed"); // TODO test this shit
                }

                lastFallSpeed = player.verticalDistance;
            } else {
                fallViolations = 0;
            }

            if (player.onGround && !player.wasOnGround && (landTicks <= 20 || player.belowBlock)) {
                landTicks = 30;
            }

            climbViolations = 0;
        }

        if (player.horizontalDistance > 0) { // TODO fix false positives, such as stairs, ice, etc.
            int speedLevel = player.getPotionLevel(PotionEffectType.SPEED);
            double speed = player.horizontalDistance / (1.0 + 0.2 * speedLevel) - player.horizontalVelocity;

            if (speed > 0.7) {
                onViolation("moving too fast (hop)");
            } else if (speed > 0.5) {
                if (nextJump == 0 || player.belowBlock) {
                    nextJump = 8;
                    jumpTicks = 11;
                } else {
                    onViolation("moving too fast (jump)");
                }
            } else if (speed > 0.33) {
                if (jumpTicks == 0 && nextJump > 0) {
                    onViolation("moving too fast (land)");
                }
            } else if (speed > 0.29) {
                if (landTicks == 0) {
                    onViolation("moving too fast (ground)");
                }
            }
        }

        if (jumpTicks > 0) {
            jumpTicks--;
        }

        if (landTicks > 0) {
            landTicks--;
        }

        if (nextJump > 0) {
            nextJump--;
        }
    }

}
