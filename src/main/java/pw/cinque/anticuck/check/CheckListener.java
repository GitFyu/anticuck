package pw.cinque.anticuck.check;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerVelocityEvent;
import org.bukkit.util.Vector;
import pw.cinque.anticuck.ACPlayer;
import pw.cinque.anticuck.util.LocationUtil;

public class CheckListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onMove(PlayerMoveEvent event) {
        if (event.isCancelled()) {
            return;
        }

        Location from = event.getFrom();
        Location to = event.getTo();

        if (!from.getWorld().equals(to.getWorld()) || to.getY() < 0.0 || to.getY() >= to.getWorld().getMaxHeight()) {
            return;
        }

        double dx = to.getX() - from.getX();
        double dy = to.getY() - from.getY();
        double dz = to.getZ() - from.getZ();

        Player bukkitPlayer = event.getPlayer();
        ACPlayer player = ACPlayer.getByPlayer(bukkitPlayer);

        if (player.velocityCounter > 0) {
            player.velocityCounter--;
        } else {
            if ((player.horizontalVelocity *= 0.95) < 0.01) {
                player.horizontalVelocity = 0.0;
            }

            if ((player.verticalVelocity *= 0.95) < 0.01) {
                player.verticalVelocity = 0.0;
            }
        }

        // it appears that PlayerVelocityEvent doesn't get called when the player is in creative mode
        if (dx == 0.0 && dy == 0.0 && dz == 0.0 || bukkitPlayer.getGameMode() == GameMode.CREATIVE || bukkitPlayer.getAllowFlight() || bukkitPlayer.isInsideVehicle()) {
            return;
        }

        int locationInfo = LocationUtil.checkLocation(to);

        player.horizontalDistance = Math.sqrt(dx * dx + dz * dz);
        player.verticalDistance = dy;
        player.wasOnGround = player.onGround;
        player.onGround = LocationUtil.isOnGround(locationInfo);
        player.swimming = LocationUtil.isSwimming(locationInfo);
        player.climbingLadder = LocationUtil.isClimbingLadder(locationInfo);
        player.inWeb = LocationUtil.isInWeb(locationInfo);
        player.belowBlock = LocationUtil.isBelowBlock(locationInfo);
        player.aboveGround = LocationUtil.isAboveGround(locationInfo);

        if (player.horizontalVelocity < 1.5 && player.verticalVelocity < 0.8) { // not much point in running the check if the player has a lot of velocity
            player.getFlyCheck().run();
        }

        player.getTooManyMovesCheck().run();
    }

    @EventHandler
    public void onVelocity(PlayerVelocityEvent event) {
        ACPlayer player = ACPlayer.getByPlayer(event.getPlayer());
        Vector velocity = event.getVelocity();

        player.velocityCounter = 20;
        player.horizontalVelocity = Math.sqrt(velocity.getX() * velocity.getX() + velocity.getZ() * velocity.getZ());
        player.verticalVelocity = Math.abs(velocity.getY());
    }

}
