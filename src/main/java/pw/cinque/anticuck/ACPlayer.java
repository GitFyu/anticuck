package pw.cinque.anticuck;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import pw.cinque.anticuck.check.move.FlyCheck;
import pw.cinque.anticuck.check.move.TooManyMovesCheck;

import java.util.Map;
import java.util.WeakHashMap;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ACPlayer {

    private static Map<Player, ACPlayer> players = new WeakHashMap<>();

    @Getter
    private final Player player;
    @Getter
    private final FlyCheck flyCheck = new FlyCheck(this);
    @Getter
    private final TooManyMovesCheck tooManyMovesCheck = new TooManyMovesCheck(this);

    public boolean punished;
    public int velocityCounter;
    public double horizontalVelocity, verticalVelocity;
    public double horizontalDistance, verticalDistance;
    public boolean onGround, wasOnGround, swimming, climbingLadder, inWeb, belowBlock, aboveGround;

    public static ACPlayer getByPlayer(Player player) {
        return players.computeIfAbsent(player, ACPlayer::new);
    }

    public int getPotionLevel(PotionEffectType type) {
        if (!player.hasPotionEffect(type)) {
            return 0;
        }

        for (PotionEffect effect : player.getActivePotionEffects()) {
            if (effect.getType().equals(type)) {
                return effect.getAmplifier() + 1;
            }
        }

        return 0;
    }

}
